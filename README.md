# ERC1155D optimized from ERC1155 without enumerable
This is a project used to compare the new proposal of ERC1155 optimized for low gas transactions.
ERC1155D is a record setter for both minting and transfer gas efficiency. It can be very used for PFP NFTs.
This contract is has not been audited yet. I would not recommend to use it in production unless you know what you are doing. This is only to demonstrate how the functionality works.

## Installation

If you are cloning the project then run this first, otherwise you can download the source code on the release page and skip this step.

```bash
git clone git@gitlab.com:xevimacia/erc1155d-optimized-smartcontract.git
```
Also create a `.env` file with the following variables:
```Python
# metamask test account keys
export PRIVATE_KEY={Your_key}
# Etherscan api key
export ETHERSCAN_TOKEN={your_API_key}
# To correct Error HH604: Error running JSON-RPC server: error:0308010C:digital envelope routines::unsupported
export NODE_OPTIONS=--openssl-legacy-provider
```
## Run the vanilla OpenZeppelin ERC1155 with few optimizations
This smartcontract based on the vanilla OpenZeppelin ERC1155 library is already optimized as below:
- currentIndex is read and written into storage once
- no <= or >= operators are used
- index starts at 1 instead of zero to avoid the costly zero to one storage update. Supply goes from 1 to 10, rather than 0 to 9 and max supply is 10.
- use _mint instead of _safeMint to avoid unnecessary checks
- The use of unchecked where overflow is impossible
- Price and supply are constant to avoid reading from storage
- Optimizer is set to 10,000. 
```bash
Running 'scripts/deploy.py::main'...
Transaction sent: 0x27024d414a6afacb0a2161ece8052844a9a5034c5b40e9458f3fa61991dab1ca
  Gas price: 0.0 gwei   Gas limit: 30000000   Nonce: 10
  ERC1155NFT.constructor confirmed   Block: 11   Gas used: 2055220 (6.85%)
  ERC1155NFT deployed at: 0x610178dA211FEF7D417bC0e6FeD39F05609AD788

Transaction sent: 0xfc6a17f4537db3a50e4e9342fcec55d9beb66387d3bb23622a27350e96561fd8
  Gas price: 0.0 gwei   Gas limit: 30000000   Nonce: 11
  ERC1155NFT.mint confirmed   Block: 12   Gas used: 51860 (0.17%)

======= Gas profile =======

ERC1155NFT <Contract>
   ├─ constructor -  avg: 2055220  avg (confirmed): 2055220  low: 2055220  high: 2055220
   └─ mint        -  avg:   51860  avg (confirmed):   51860  low:   51860  high:   51860
```
**gas cost to mint: 51,860 (optimizer set to 10,000)**
## Removing unnecessary features from OpenZeppelin
For use case specific to PFP projects we assume the minting isn’t delegated to another address and the recipient of the NFT is a user wallet, not a smart contract.
We made changes to the vanilla OpenZeppelin ERC1155 library like:
```solidity
function _mint(
        address to,
        uint256 id,
        uint256 amount,
        bytes memory data
    ) internal virtual {
        require(to != address(0), "ERC1155: mint to the zero address"); // Remove as msg.sender can never be the zero address

        address operator = _msgSender(); // Remove. We assume there are no delegated mints
        // ids and amounts are only used for the _beforeTokenTransfer and _afterTokenTransfer
        uint256[] memory ids = _asSingletonArray(id); // Remove 
        uint256[] memory amounts = _asSingletonArray(amount); // Remove


        _beforeTokenTransfer(operator, address(0), to, ids, amounts, data); // Remove

        _balances[id][to] += amount;
        emit TransferSingle(operator, address(0), to, id, amount);

        _afterTokenTransfer(operator, address(0), to, ids, amounts, data); // Remove

        _doSafeTransferAcceptanceCheck(operator, address(0), to, id, amount, data); // Remove
    }
```
We can take this a step further. If we convert _balances to be an internal rather than private variable, we do not need to call the costly mint() function and just directly update _balances and emit the appropriate event in our ERC1155D smart contract. So we change:
from ERC1155
```solidity
// Mapping from token ID to account balances
mapping(uint256 => mapping(address => uint256)) internal _balances;
```
from ERC1155D, we remove the mint() function and add:
```solidity
_balances[_currentIndex][msg.sender] = 1;
emit TransferSingle(msg.sender, address(0), msg.sender, _currentIndex, 1);
```
Note: we set _balances to be equal to 1 rather than doing a += operation. We know the balance for a particular id will either be 1 or 0.
```bash
Running 'scripts/deploy.py::main'...
Transaction sent: 0x315651d6f12309d99125a73d1b59774facb756bad2804fc9188f218221db1789
  Gas price: 0.0 gwei   Gas limit: 30000000   Nonce: 16
  ERC1155NFT.constructor confirmed   Block: 17   Gas used: 1995703 (6.65%)
  ERC1155NFT deployed at: 0x959922bE3CAee4b8Cd9a407cc3ac1C251C2007B1

Transaction sent: 0xea56440d957169141d02a8578c4b9d32369fec4002d1dc7643f15351ef9d9013
  Gas price: 0.0 gwei   Gas limit: 30000000   Nonce: 17
  ERC1155NFT.mint confirmed   Block: 18   Gas used: 51025 (0.17%)

======= Gas profile =======

ERC1155NFT <Contract>
   ├─ constructor -  avg: 1995703  avg (confirmed): 1995703  low: 1995703  high: 1995703
   └─ mint        -  avg:   51025  avg (confirmed):   51025  low:   51025  high:   51025
```
**gas cost to mint: 51,025 (optimizer set to 10,000)**

## Transitioning from ERC1155 to ERC1155D
We made the following changes:
1. Override balanceof function 
```solidity
function balanceOf(address account, uint256 id) public view virtual override returns (uint256) {
        require(account != address(0), "ERC1155: address zero is not a valid owner");
        require(id < MAX_SUPPLY, "ERC1155: id exceeds maximum");
        return _owners[id] == account ? 1 : 0;
    }
```
2. Double map _balances[_currentIndex][address]from OpenZeppelin is only storing 0 or 1. We replace it by an array of addresses called _owners.
```solidity
uint256 public constant MAX_SUPPLY = 5000; // e.g. collection size
address[MAX_SUPPLY] internal _owners;
```
and replace
```solidity
_balances[_currentIndex][msg.sender] = 1;
```
for
```solidity
_owners[_currentIndex] = msg.sender;
```
We will get:
```bash
Running 'scripts/deploy.py::main'...
Transaction sent: 0xe51529c81911475909d3b4a673c611b5e2782c9addb598e80ebe2d6d23a938a1
  Gas price: 0.0 gwei   Gas limit: 30000000   Nonce: 0
  ERC1155NFT.constructor confirmed   Block: 1   Gas used: 2047364 (6.82%)
  ERC1155NFT deployed at: 0x5FbDB2315678afecb367f032d93F642f64180aa3

Transaction sent: 0x09ba5e7fcf8f2e3a66971dc2f478e34d2621fa7afc26e02628356902a7c7fcbe
  Gas price: 0.0 gwei   Gas limit: 30000000   Nonce: 1
  ERC1155NFT.mint confirmed   Block: 2   Gas used: 50954 (0.17%)

======= Gas profile =======

ERC1155NFT <Contract>
   ├─ constructor -  avg: 2047364  avg (confirmed): 2047364  low: 2047364  high: 2047364
   └─ mint        -  avg:   50954  avg (confirmed):   50954  low:   50954  high:   50954
```
**gas cost to mint: 50,954 (optimizer set to 10,000)**
## Adding a bit of assembly code to save more gas
replace
```solidity
_owners[_currentIndex] = msg.sender;
```
for
```solidity
assembly {
  sstore(add(_owners.slot, _currentIndex), caller())
}
```
We will get:
```bash
Running 'scripts/deploy.py::main'...
Transaction sent: 0xe6077b10924d8e52c96b20d881ae0a26913c0842d6754d60971e423de4f0d123
  Gas price: 0.0 gwei   Gas limit: 30000000   Nonce: 0
  ERC1155NFT.constructor confirmed   Block: 1   Gas used: 2003378 (6.68%)
  ERC1155NFT deployed at: 0x5FbDB2315678afecb367f032d93F642f64180aa3

Transaction sent: 0x09ba5e7fcf8f2e3a66971dc2f478e34d2621fa7afc26e02628356902a7c7fcbe
  Gas price: 0.0 gwei   Gas limit: 30000000   Nonce: 1
  ERC1155NFT.mint confirmed   Block: 2   Gas used: 50892 (0.17%)

======= Gas profile =======

ERC1155NFT <Contract>
   ├─ constructor -  avg: 2003378  avg (confirmed): 2003378  low: 2003378  high: 2003378
   └─ mint        -  avg:   50892  avg (confirmed):   50892  low:   50892  high:   50892
```
**gas cost to mint: 50,892 (optimizer set to 10,000)**
## Compare ERC1155 (without enumerable) and ERC1155D mint and transfer functions
For the comparisson, I edited 3 smartcontract functions and moved some code to customized ERC1155 smartcontract.
Moved the following code to ERC1155:
```Solidity
uint256 public constant MAX_SUPPLY = 5000; // e.g. collection size
    uint256 public constant MAX_ID_PLUS_ONE = 5001;
    address[MAX_SUPPLY] internal _owners; // slightly more efficient to initialize the array to the maximum supply
```
Added balanceOfOpt, _safeTransferFromOpt function in the ERC1155 contract:
```Solidity
function balanceOfOpt(address account, uint256 id) public view virtual returns (uint256) {
  require(account != address(0), "ERC1155: balance query for the zero address");
  require(id < MAX_SUPPLY, "ERC1155D: id exceeds maximum");
  
  return _owners[id] == account ? 1 : 0;
}

function _safeTransferFromOpt(
  address from,
  address to,
  uint256 id,
  uint256 amount,
  bytes memory data
) internal virtual {
  require(to != address(0), "ERC1155: transfer to the zero address");

  address operator = _msgSender();
  uint256[] memory ids = _asSingletonArray(id);
  uint256[] memory amounts = _asSingletonArray(amount);

  _beforeTokenTransfer(operator, from, to, ids, amounts, data);

  require(_owners[id] == from && amount < 2, "ERC1155: insufficient balance for transfer");

  // The ERC1155 spec allows for transfering zero tokens, but we are still expected
  // to run the other checks and emit the event. But we don't want an ownership change
  // in that case
  if (amount == 1) {
    _owners[id] = to;
  }

  emit TransferSingle(operator, from, to, id, amount);

  _afterTokenTransfer(operator, from, to, ids, amounts, data);

  _doSafeTransferAcceptanceCheck(operator, from, to, id, amount, data);
}
```
Edited ERC1155D and created the following functions:
1. mintOptimized()
2. safeTransferFromOpt()
3. mintOriginal

Finally, I deploy, mint and transfer original ERC1155 smartcontract without enumerable and the optimized ERC1155D to compare gas fees. Results below:
```bash
Running 'scripts/deploy.py::main'...
Deploy without optimization
Transaction sent: 0x4a1736c9de2badd49502aaf97e44b2c1338d791f80abe610c95299967852d077
  Gas price: 0.0 gwei   Gas limit: 30000000   Nonce: 77
  ERC1155NFT.constructor confirmed   Block: 78   Gas used: 2274204 (7.58%)
  ERC1155NFT deployed at: 0x0355B7B8cb128fA5692729Ab3AAa199C1753f726

Transaction sent: 0x8501989e7d00fd59d6b1dc2c59316d663b1423e86c6135a60efffc480536a84b
  Gas price: 0.0 gwei   Gas limit: 30000000   Nonce: 78
  ERC1155NFT.mintOriginal confirmed   Block: 79   Gas used: 49923 (0.17%)

Transaction sent: 0xbc0bcaa7a0dabd2e74c4361d4a44766639191f5bcada192de7cc75bd7e28ab72
  Gas price: 0.0 gwei   Gas limit: 30000000   Nonce: 79
  ERC1155NFT.safeTransferFrom confirmed   Block: 80   Gas used: 51907 (0.17%)

Deploy optimized
Transaction sent: 0x9d583d5feaca3a126c0a7028cdea37811793bfbc50a70f70cee5bfbf5b9ac94f
  Gas price: 0.0 gwei   Gas limit: 30000000   Nonce: 80
  ERC1155NFT.constructor confirmed   Block: 81   Gas used: 2274204 (7.58%)
  ERC1155NFT deployed at: 0x172076E0166D1F9Cc711C77Adf8488051744980C

Transaction sent: 0xd1abcfa32d6da135c68f268ea740969eb94f2dea4b4a243ae92d8da580e74aac
  Gas price: 0.0 gwei   Gas limit: 30000000   Nonce: 81
  ERC1155NFT.mintOptimized confirmed   Block: 82   Gas used: 50916 (0.17%)

Transaction sent: 0x4d1b2e17f52a0e367cf7841b1cdbff20f3ebefb04af144d2d4960583d6db7d8c
  Gas price: 0.0 gwei   Gas limit: 30000000   Nonce: 82
  ERC1155NFT.safeTransferFromOpt confirmed   Block: 83   Gas used: 36508 (0.12%)

======= Gas profile =======

ERC1155NFT <Contract>
   ├─ constructor         -  avg: 2274204  avg (confirmed): 2274204  low: 2274204  high: 2274204
   ├─ safeTransferFrom    -  avg:   51907  avg (confirmed):   51907  low:   51907  high:   51907
   ├─ mintOptimized       -  avg:   50916  avg (confirmed):   50916  low:   50916  high:   50916
   ├─ mintOriginal        -  avg:   49923  avg (confirmed):   49923  low:   49923  high:   49923
   └─ safeTransferFromOpt -  avg:   36508  avg (confirmed):   36508  low:   36508  high:   36508
```
**gas cost to mintOriginal: 49,923 (optimizer set to 10,000)**

**gas cost to mintOptimized: 50,916 (optimizer set to 10,000)**

**gas cost to safeTransferFrom: 51,907 (optimizer set to 10,000)**

**gas cost to safeTransferFromOpt: 36,508 (optimizer set to 10,000)**

The difference in minting between original and optimized ERC1155 is around 1,000 but ERC1155D has the huge advantage to cut transfer cost to about 15,400. The main reason is because transferring a token doesn't need to update two balances with an address, it just changges the address stored in a particular index.

ERC1155D transfer costs are ~36,500 gas, which may even be more efficient than sending ERC20 tokens! Compared to the average NFT, ERC1155D cuts the transfer cost 40%. Furthermore, because it is an ERC1155, it is capable of transferring multiple token ids in one transaction. So the savings are much higher.

"By comparison, ERC721Enumerable costs about 89,000 gas for a transfer, ERC721A costs at least 62,000 gas, and ERC721 slim costs 38,000 gas. ERC20 tokens cost between 29,000 and 52,000 gas depending on the prior balances of the addresses.

This is an extremely important advantage. With low transfer costs, secondary buyers are much more likely to complete the purchase."

Let's check the numbers in terms of $. Just to recap the formula:

***gas cost x gas price (gwei) x ETH price (dollars) / 1 billion***

*Note: We will use 30 Gwei (gas price) as standard and current ETH price at $2,000. It is worth noting that this is something developers cannot control.*

**Cost to mintOriginal = 49,923 x 30 x 2,000 / 1 billion = $2.995**

*Cost to mintOriginal (with network congested 200 Gwei and ATH eth 4,8000) =$47.92*

**Cost to mintOptimized: 50,916 x 30 x 2,000 / 1 billion = $3.055**

*Cost to mintOptimized (with network congested 200 Gwei and ATH eth 4,8000) = $48.88*

**Cost to safeTransferFrom: 51,907 x 30 x 2,000 / 1 billion = $3.114**

*Cost to safeTransferFrom (with network congested 200 Gwei and ATH eth 4,8000) = $49.824*

**Cost to safeTransferFromOpt: 36,508 x 30 x 2,000 / 1 billion = $2.190**

*Cost to safeTransferFromOpt (with network congested 200 Gwei and ATH eth 4,8000) = $35.04*

We have a big difference of almost ~$15 (~30%) in extreme cases. This is only comparing ERC1155 vs ERC1155D. In the case of ERC721, the costs are even higher and thus more savings for secondary market sales.

For more details, you can check the article that inspired this repo [here](https://medium.com/donkeverse/introducing-erc1155d-the-most-efficient-non-fungible-token-contract-in-existence-c1d0a62e30f1).

Thank you!

## Helpful commands

Install hardhat
```bash
npm install --save-dev hardhat
```

Run hardhat node
```bash
npx hardhat node
```

Compile with brownie
```bash
brownie compile
```

Test with gas and coverage (No tests implemented in this repo)
```bash
brownie test tests/unit/test.py -s --gas --coverage --network hardhat
```

Run deployment scripts
```bash
brownie run scripts/deploy.py --gas --coverage --network hardhat
```

Get brownie network lists
```bash
brownie networks list true
```