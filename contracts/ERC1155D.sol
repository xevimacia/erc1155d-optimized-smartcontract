// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.10;

import "./ERC1155.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract ERC1155NFT is ERC1155, Ownable {
    using Strings for uint256;

    uint256 public constant PRICE = 0.05 ether;
    uint256 public currentIndex = 1;
    string public name = "collection name";
    string public baseURI = "ipfs://.../";

    constructor() ERC1155("") {}

    function mintOptimized() external payable {
        uint256 _currentIndex = currentIndex;
        require(_currentIndex < MAX_ID_PLUS_ONE);
        require(msg.value == PRICE, "wrong price");
        require(msg.sender == tx.origin, "no smart contracts, only EOA");

        emit TransferSingle(msg.sender, address(0), msg.sender, _currentIndex, 1);
        // equivalent to
        // _owners[_currentIndex] = msg.sender;
        // saves some more gas
        assembly {
            sstore(add(_owners.slot, _currentIndex), caller())
        }

        unchecked {
            _currentIndex++;
        }
        currentIndex = _currentIndex;
    }

    function safeTransferFromOpt(
        address from,
        address to,
        uint256 id,
        uint256 amount,
        bytes memory data)
        public
        onlyOwner
    {
        _safeTransferFromOpt(from, to, id, amount, data);
    }

    function mintOriginal(address account, uint256 id, uint256 amount, bytes memory data)
        external payable
        onlyOwner
    {
        _mint(account, id, amount, data);
    }

    function uri(uint256 id) public view virtual override returns(string memory) {
        require(id <= MAX_ID_PLUS_ONE, "invalid id");
        return string(abi.encodePacked(baseURI, id.toString()));
    }
    
}