
from brownie import ERC1155NFT, accounts

from scripts.helpful_scripts import get_account

from web3 import Web3

# Marketplace smart contract addresses
# Opensea ERC1155
# https://github.com/ProjectOpenSea/opensea-creatures/blob/master/migrations/2_deploy_contracts.js
# https://ethereum.stackexchange.com/questions/122237/why-opensea-polygon-proxy-contract-does-not-have-transactions
rinkeby_os_proxy = "0x1E525EEAF261cA41b809884CBDE9DD9E1619573A"
mainnet_os_proxy = "0xa5409ec958c83c3f309868babaca7c86dcb077c1"
polygon_os_proxy = "0x207Fa8Df3a17D96Ca7EA4f2893fcdCb78a304101"
# Rarible ERC1155
# https://docs.rarible.org/reference/contract-addresses/
rinkeby_rarible_proxy = "0x1AF7A7555263F275433c6Bb0b8FdCD231F89B1D7"
mainnet_rarible_proxy = "0xB66a603f4cFe17e3D27B87a8BfCaD319856518B8"

"""
Smart contract level arguments
@params:
    name: name of the collection,
    symbol: symbol of the collection,
    uri: base uri, can be changed at any time. Also can use custom uri for each token id
    total_supply: maximum number of token id
    edition_limit: maximum number of tokens minted per token id
    newRoyaltyPercent: % of royalties to the creator by x / 10000
    contract_uri: Upon importing the contract to OpenSea, contract-level metadata will now pre-populate in the applicable collection fields. 
    proxyRegistryAddress: Whitelisting the OpenSea proxy contract address enables NFT buyers to list on OpenSea without paying gas fees.
"""

def deploy_original():
    account = get_account()
    nft = ERC1155NFT.deploy({"from": account})
    nft.mintOriginal(account, 1, 1, "",{"from": account, "value": Web3.toWei(0.05, "ether")})
    nft.safeTransferFrom(account, accounts[1], 1, 1, "", {"from": get_account()})
    # minting account should have 0 tokens of type 1
    assert nft.balanceOf(account, 1) == 0
    # new account should have 1 token of type 1
    assert nft.balanceOf(accounts[1], 1) == 1
    return

def deploy_optimized():
    account = get_account()
    nft = ERC1155NFT.deploy({"from": account})
    nft.mintOptimized({"from": account, "value": Web3.toWei(0.05, "ether")})
    nft.safeTransferFromOpt(account, accounts[1], 1, 1, "", {"from": account})
    # minting account should have 0 tokens of type 1
    assert nft.balanceOfOpt(account, 1) == 0
    # new account should have 1 token of type 1
    assert nft.balanceOfOpt(accounts[1], 1) == 1
    return


def main():
    print("Deploy without optimization")
    deploy_original()
    print("Deploy optimized")
    deploy_optimized()
