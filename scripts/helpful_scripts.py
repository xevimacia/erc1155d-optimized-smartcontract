from brownie import accounts, network, config
import requests

LOCAL_BLOCKCHAIN_ENVIRONMENTS = ["hardhat", "development", "ganache", "mainnet-fork"]
OPENSEA_FORMAT = "https://testnets.opensea.io/assets/{}/{}"

def get_account(index=None, id=None):
    if index:
        return accounts[index]
    if network.show_active() in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        return accounts[0]
    if id:
        return accounts.load(id)
    return accounts.add(config["wallets"]["from_key"])

def get_event_value(_tx, _event_name: str, _key: str):
    return _tx.events[_event_name][_key]


def check_token_uri(contract_address, total_supply):
    headers = {"Accept": "application/json"}
    opensea_uri = "https://testnets-api.opensea.io/asset/"
    token_id = 1
    while token_id <= total_supply:
        uri = f"{opensea_uri}{contract_address}/{token_id}/validate"
        response = requests.get(uri, headers=headers)
        print(response.text)
        token_id = token_id + 1